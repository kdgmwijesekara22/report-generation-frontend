import {
    PATIENTS,
    DOCTORS,
    STAFF,
    FETCH_PATIENTS_SUCCESS_ACTION,
    FETCH_DOCTORS_SUCCESS_ACTION,
    FETCH_STAFF_SUCCESS_ACTION
} from '../actions/test/test.interface';
import { TEST_ACTIONS_TYPES } from '../actions/test/test.actionTypes';

// Initial state
const initialState: {
    patients: PATIENTS,
    doctors: DOCTORS,
    staff: STAFF
} = {
    patients: {
        data: []
    },
    doctors: {
        data: []
    },
    staff: {
        data: []
    }
};

// Reducer function
const testReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case TEST_ACTIONS_TYPES.FETCH_PATIENTS_SUCCESS:
            return {
                ...state,
                patients: {
                    data: action.payload
                }
            };
        case TEST_ACTIONS_TYPES.FETCH_DOCTORS_SUCCESS:
            return {
                ...state,
                doctors: {
                    data: action.payload
                }
            };
        case TEST_ACTIONS_TYPES.FETCH_STAFF_SUCCESS:
            return {
                ...state,
                staff: {
                    data: action.payload
                }
            };
        default:
            return state;
    }
};

export default testReducer;
