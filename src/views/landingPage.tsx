import React from 'react';
import Header from '../components/product/landingPage/header';
import Projects from '../components/product/landingPage/projects';
import Demo from '../components/product/landingPage/demo';

const LandingPage = function () {
    return (
       <>
        <Header />
        <Demo />
        <Projects />
       </>
    );
}

export default LandingPage;