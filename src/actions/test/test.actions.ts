import { AxiosResponse } from 'axios';
import { Dispatch } from 'redux';
import { TEST_ACTIONS_TYPES } from './test.actionTypes';
import { Doctor, Patient, Staff, Audit } from '../../api/data';  // Importing the Audit interface
import {
    FETCH_PATIENTS_SUCCESS_ACTION,
    FETCH_DOCTORS_SUCCESS_ACTION,
    FETCH_STAFF_SUCCESS_ACTION,
    FetchActions
} from './test.interface';
import { getAllDoctors, getAllPatients, getAllStaff, getAuditDetails } from '../../api';  // Importing the getAuditDetails function

export const fetchAllPatients = () => {
    return async (dispatch: Dispatch<FetchActions>) => {
        const response: AxiosResponse<Patient[]> = await getAllPatients();
        dispatch({
            type: TEST_ACTIONS_TYPES.FETCH_PATIENTS_SUCCESS,
            payload: response.data
        });
    };
}

export const fetchAllDoctors = () => {
    return async (dispatch: Dispatch<FetchActions>) => {
        const response: AxiosResponse<Doctor[]> = await getAllDoctors();
        dispatch({
            type: TEST_ACTIONS_TYPES.FETCH_DOCTORS_SUCCESS,
            payload: response.data
        });
    };
}

export const fetchAllStaff = () => {
    return async (dispatch: Dispatch<FetchActions>) => {
        const response: AxiosResponse<Staff[]> = await getAllStaff();
        dispatch({
            type: TEST_ACTIONS_TYPES.FETCH_STAFF_SUCCESS,
            payload: response.data
        });
    };
}

export interface FETCH_AUDIT_SUCCESS_ACTION {
    type: TEST_ACTIONS_TYPES.FETCH_AUDIT_SUCCESS;
    payload: Audit;
}

// export const fetchAuditDetails = () => {
//     return async (dispatch: Dispatch<FetchActions>) => {
//         const response: AxiosResponse<Audit> = await getAuditDetails();
//         const auditData: Audit = response.data;
//         console.log(auditData)
//         dispatch({
//             type: TEST_ACTIONS_TYPES.FETCH_AUDIT_SUCCESS,
//             payload: auditData
//         });
//     };
// }

