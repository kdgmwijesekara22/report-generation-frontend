import { TEST_ACTIONS_TYPES } from './test.actionTypes';

// Interfaces for the state
export interface PATIENTS {
    data: Patient[];
};

export interface DOCTORS {
    data: Doctor[];
};

export interface STAFF {
    data: Staff[];
};

// Action types for fetching success
export interface FETCH_PATIENTS_SUCCESS_ACTION {
    type: TEST_ACTIONS_TYPES.FETCH_PATIENTS_SUCCESS;
    payload: Patient[];
};

export interface FETCH_DOCTORS_SUCCESS_ACTION {
    type: TEST_ACTIONS_TYPES.FETCH_DOCTORS_SUCCESS;
    payload: Doctor[];
};

export interface FETCH_STAFF_SUCCESS_ACTION {
    type: TEST_ACTIONS_TYPES.FETCH_STAFF_SUCCESS;
    payload: Staff[];
};

// Union type for all fetch actions
export type FetchActions = FETCH_PATIENTS_SUCCESS_ACTION
    | FETCH_DOCTORS_SUCCESS_ACTION
    | FETCH_STAFF_SUCCESS_ACTION
    | FETCH_AUDIT_SUCCESS_ACTION;


// Defining the types for Patient, Doctor, and Staff
export interface Patient {
    id: number;
    name: string;
    age: number;
}

export interface Doctor {
    id: number;
    name: string;
    specialization: string;
}

export interface Staff {
    id: number;
    name: string;
    role: string;
}
export interface Address {
    address1: string;
    address2?: string;
    address3?: string;
    address?: string;
}

export interface Body1 {
    line1: string;
    line2: string;
}

export interface TableBody {
    FundName: string;
    noOfUnits: string;
    unitPrice: string;
    marketValue: string;
    gain: string;
}

export interface Table {
    header1: string;
    header2: string;
    header3: string;
    header4: string;
    header5: string;
    tableBody: TableBody[];
    body2: string;
    body3: string;
    body4: string;
}

export interface Footer {
    faith: string;
    referenceName: string;
    space: string;
    sign: string;
}

export interface Audit {
    templateString: string;
    date: string;
    toAddress: Address;
    designation: string;
    clientName: string;
    clientNumber: string;
    body1: Body1;
    tbl: Table;
    footer: Footer;
    ccAddress: Address;
}

export interface FETCH_AUDIT_SUCCESS_ACTION {
    type: TEST_ACTIONS_TYPES.FETCH_AUDIT_SUCCESS;
    payload: Audit;
}
