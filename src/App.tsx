import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
// import { get } from './actions/test/test.actions'; // Update the path accordingly
import './App.css';
import Router from './router/index';
import {getAuditDetails} from "./api";
import theme from "./theme/theme";

function App() {
    const dispatch = useDispatch();



    return (
        <div className="App">
            <Router />
        </div>
    );
}

export default App;
