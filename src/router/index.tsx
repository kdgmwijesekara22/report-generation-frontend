import React, { Suspense, lazy } from 'react';
import { Routes, Route } from 'react-router-dom';
import LandingPage from "../views/landingPage";
import UiEditor from "../views/uiEditor";
import AuditClientConfirmation from "../components/ndb/jsx_templates/audit_client_confirmation";
import NormalConfirmation from "../components/ndb/jsx_templates/normal_confirmation";
import PerformanceReport from "../components/ndb/jsx_templates/perfomance";
import Filter_from from "../components/ndb/filter_from";

const Router: React.FC<{}> = function () {
    return (
        <Routes>
            <Route path='/' element={<LandingPage />} />
            <Route path='/editor' element={<UiEditor />} />
            <Route path='/audit' element={<AuditClientConfirmation />} />
            <Route path='/nconfirmation' element={<NormalConfirmation />} />
            <Route path='/perfomance' element={<PerformanceReport />} />
            <Route path='/filter' element={<Filter_from />} />
        </Routes>
    );
}

export default Router;