// Define types for Patients, Doctors, and Staff
export interface Patient {
    id: number;
    name: string;
    age: number;
    condition: string;
}

export interface Doctor {
    id: number;
    name: string;
    specialization: string;
}

export interface Staff {
    id: number;
    name: string;
    role: string;
}

// Define dummy data for Patients, Doctors, and Staff
export const patients: Patient[] = [
    {id: 1, name: "John Doe", age: 32, condition: "Flu"},
    {id: 2, name: "Jane Smith", age: 28, condition: "Sprained Ankle"},
    {id: 3, name: "Robert Brown", age: 45, condition: "High Blood Pressure"}
];

interface Address {
    address1: string;
    address2?: string;
    address3?: string;
    address?: string;
}

interface CCAddress {
    addressLine1: string;
    addressLine2?: string;
}

interface Body1 {
    line1: string;
    line2: {
        line1: string;
        dueDate: string;
        line2: string;
    };
}

interface TableBody {
    FundName: string;
    noOfUnits: string;
    unitPrice: string;
    marketValue: string;
    gain: string;
}

interface Table {
    header1: string;
    header2: string;
    header3: string;
    header4: string;
    header5: string;
    tableBody: TableBody[];
}

interface Body2 {
    line1: string;
    line2: string;
    line3: string;
}

interface Footer {
    faith: string;
    referenceName: string;
    space: string;
    sign: string;
}

export interface Audit {
    date: string;
    toAddress: Address;
    designation: string;
    clientName: string;
    clientNumber: string;
    body1: Body1;
    tbl: Table;
    body2: Body2;
    footer: Footer;
    ccAddress: CCAddress;
}


export const doctors: Doctor[] = [
    {id: 1, name: "Dr. Emily White", specialization: "Cardiology"},
    {id: 2, name: "Dr. Michael Johnson", specialization: "Pediatrics"},
    {id: 3, name: "Dr. Olivia Wilson", specialization: "Neurology"}
];

export const staff: Staff[] = [
    {id: 1, name: "Lucas Taylor", role: "Nurse"},
    {id: 2, name: "Sophia Lewis", role: "Receptionist"},
    {id: 3, name: "Ethan Walker", role: "Lab Technician"}
];
