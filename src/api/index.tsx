import axios, { AxiosInstance, AxiosResponse } from 'axios';
import {patients, doctors, staff, Audit} from './data';

// Define the base API URL
const API_URL = 'https://a3aa1a43-05c3-4885-b589-ea67f48f1a34.mock.pstmn.io/api/v1';

// Define interfaces for expected response shapes and request parameters
interface Book {
    title: string;
    author: string;
    isbn: string;
}

interface Patient {
    id: number;
    name: string;
    age: number;
    condition: string;
}

interface Doctor {
    id: number;
    name: string;
    specialization: string;
}

interface Staff {
    id: number;
    name: string;
    role: string;
}

// Create an Axios instance
const axiosInstance: AxiosInstance = axios.create({
    baseURL: API_URL
});

// Set up request and response interceptors
axiosInstance.interceptors.request.use(config => {
    // Do any request configurations or modifications here
    return config;
});

axiosInstance.interceptors.response.use(response => {
    // Handle response or errors here
    return response;
});

// Define utility functions for API operations

const getAllBooks = async (): Promise<AxiosResponse<Book[]>> => {
    // Mocking data response for books (assuming you'll have a books array in data.tsx later)
    return {data: [], status: 200, statusText: 'OK', headers: {}, config: {}};
};

const getBookByIsbn = async (isbn: string): Promise<AxiosResponse<Book>> => {
    // Mocking data response for a single book (assuming you'll have a find function for books in data.tsx later)
    return {data: {title: '', author: '', isbn: isbn}, status: 200, statusText: 'OK', headers: {}, config: {}};
};

const getAllPatients = async (): Promise<AxiosResponse<Patient[]>> => {
    return {data: patients, status: 200, statusText: 'OK', headers: {}, config: {}};
};

const getAllDoctors = async (): Promise<AxiosResponse<Doctor[]>> => {
    return {data: doctors, status: 200, statusText: 'OK', headers: {}, config: {}};
};

const getAllStaff = async (): Promise<AxiosResponse<Staff[]>> => {
    return {data: staff, status: 200, statusText: 'OK', headers: {}, config: {}};
};

const getAuditDetails = async (): Promise<AxiosResponse<Audit>> => {
    console.log("call")
    return await axiosInstance.get<Audit>('/report-generator/get/values/report/1/client/1');
};

export {
    getAllBooks,
    getBookByIsbn,
    getAllPatients,
    getAllDoctors,
    getAllStaff,
    getAuditDetails
};
