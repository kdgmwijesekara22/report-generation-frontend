import CssBaseline from '@mui/material/CssBaseline'
import Container from '@mui/material/Container'
import Paper from '@mui/material/Paper'
import Footer from "./components/Footer";
import StepForm from "./components/StepForm";
import Header from "./components/Header";
import React from 'react';

export default function Filter_from() {
    return (
        <>
            <CssBaseline/>
            <Header/>
            <Container component='main' maxWidth='md' sx={{mb: 4}}>
                <Paper variant='outlined' sx={{my: {xs: 3, md: 6}, p: {xs: 2, md: 3}}}>
                    <StepForm/>
                </Paper>
                <Footer/>
            </Container>
        </>
    )
}
