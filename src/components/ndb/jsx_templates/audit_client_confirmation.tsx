import React, {useEffect, useState} from 'react';
import { Typography, Container, Paper, Table, TableBody, TableCell, TableRow, Divider } from '@mui/material';
import {getAuditDetails} from "../../../api";
import {Audit} from "../../../api/data";


const AuditClientConfirmation: React.FC = () => {
    const [auditData, setAuditData] = useState<Audit | null>(null);

    useEffect(() => {
        getAuditDetails()
            .then(response => {
                setAuditData(response.data);  // <-- Extract the data from the Axios response here
            })
            .catch(error => {
                console.error("Error fetching audit details:", error);
            });
    }, []);

    return (
        <Container maxWidth="md" style={{ textAlign: 'left', justifyContent: 'left' }}>
            <Paper elevation={3} style={{ padding: '20px', marginTop: '20px', textAlign: 'left' }}>
            <Typography variant="body1" >{auditData?.date || ''}</Typography>

                <Typography variant="body1" gutterBottom style={{ fontWeight: 'bold', marginTop: '20px' }}>
                    {auditData?.clientName || ''}<br />
                    {auditData?.toAddress.address1 || ''}<br />
                    {auditData?.toAddress.address2 || ''}<br />
                    {auditData?.toAddress.address3 || ''}<br />
                    {auditData?.toAddress.address || ''}
                </Typography>

                <Typography variant="body1"  gutterBottom style={{ marginTop: '20px' }}>
                    {auditData?.designation || ''}
                </Typography>

                <Typography variant="body1" gutterBottom style={{ fontWeight: 'bold', marginTop: '20px' }}>
                    CONFIRMATION OF UNITHOLDING - {auditData?.clientNumber || ''}
                </Typography>

                <Typography variant="body1" gutterBottom style={{ fontWeight: 'bold', marginTop: '20px', textDecoration: 'underline', textDecorationColor: 'black' }}>
                    {auditData?.clientName || ''} - {auditData?.clientNumber || ''}
                </Typography>


                <Typography variant="body1" paragraph style={{ marginTop: '20px'}}>
                    With reference to the request made by <span style={{ fontWeight: 'bold' }}>SENKADAGALA FINANCE PLC</span>,SENKADAGALA FINANCE PLC, we wish to confirm the following investments.
                </Typography>

                <Typography variant="body1" paragraph>
                    As at of <span style={{ fontWeight: 'bold' }}>March 31, 2023</span>, total number of units held in the name of <span style={{ fontWeight: 'bold' }}>SENKADAGALA FINANCE PLC</span>, in the
                    Unit Capital of the NDB WEALTH MANAGEMENT LIMITED Unit Trust Funds and the respective market
                    values are as follows.
                </Typography>


                <Table style={{ borderCollapse: 'collapse' }}>
                    <TableBody>
                        <TableRow>
                            <TableCell style={{ border: '1px solid black', fontWeight: 'bold' }}>{auditData?.tbl.header1}</TableCell>
                            <TableCell style={{ border: '1px solid black', fontWeight: 'bold' }}>{auditData?.tbl.header2}</TableCell>
                            <TableCell style={{ border: '1px solid black', fontWeight: 'bold' }}>{auditData?.tbl.header3}</TableCell>
                            <TableCell style={{ border: '1px solid black', fontWeight: 'bold' }}>{auditData?.tbl.header4}</TableCell>
                            <TableCell style={{ border: '1px solid black', fontWeight: 'bold' }}>{auditData?.tbl.header5}</TableCell>
                        </TableRow>
                        {auditData?.tbl.tableBody.map((row, index) => (
                            <TableRow key={index}>
                                <TableCell style={{ border: '1px solid black' }}>{row.FundName}</TableCell>
                                <TableCell style={{ border: '1px solid black' }}>{row.noOfUnits}</TableCell>
                                <TableCell style={{ border: '1px solid black' }}>{row.unitPrice}</TableCell>
                                <TableCell style={{ border: '1px solid black' }}>{row.marketValue}</TableCell>
                                <TableCell style={{ border: '1px solid black' }}>{row.gain}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

                <Typography variant="body1" paragraph style={{ marginTop: '20px' }}>
                    {auditData?.body2.line1}
                </Typography>

                <Typography variant="body1" paragraph style={{ marginTop: '20px' }}>
                    {auditData?.body2.line2}
                </Typography>

                <Typography variant="body1" paragraph style={{ marginTop: '20px' }}>
                    {auditData?.body2.line3}
                </Typography>

                {/*footer*/}
                {/*footer*/}
                <Typography variant="body1" paragraph style={{ marginTop: '20px' }}>
                    This letter has been issued on request of the above account holder without any liability on our part.
                </Typography>

                <Typography variant="body1" paragraph>
                    {auditData?.footer.faith}<br />
                    <span style={{ fontWeight: 'bold' }}>{auditData?.footer.referenceName}</span>
                </Typography>

                <Typography variant="body1">
                    {auditData?.footer.space}<br />
                    {auditData?.footer.sign}
                </Typography>

                <Typography variant="body1" paragraph style={{ marginTop: '30px' }}>
                    <span style={{ fontWeight: 'bold' }}>{auditData?.ccAddress.addressLine1}</span><br />
                    <span style={{ fontWeight: 'bold' }}>{auditData?.ccAddress.addressLine2}</span>
                </Typography>


            </Paper>
        </Container>
    );
}

export default AuditClientConfirmation;