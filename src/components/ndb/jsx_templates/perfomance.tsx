import React, { FC } from 'react';
import { Typography, Paper, Container, Divider, Box } from '@mui/material';





function PerformanceReport() {
    return (
        <Container maxWidth="sm">
            <Paper elevation={3} style={{ padding: '32px', marginTop: '16px' }}>
                <Typography align="center" style={{  fontSize: '1.2rem', marginBottom: '0px' }}>
                    NDB Wealth Management Ltd.
                </Typography>
                <Typography align="center" style={{ fontWeight: 'bold', fontSize: '1.2rem' }}>
                    PERFORMANCE REPORT
                </Typography>
                <Typography align="center" style={{ fontWeight: 'bold',fontSize: '1.2rem' }}>
                    NET OF FEES
                </Typography>
                <Typography align="center" variant="body2" style={{fontWeight: 'bold', fontSize: '1.05rem', fontStyle: 'italic' }}>
                    M.W.A.C MATHTHAMAGODA
                </Typography>
                <Typography align="center" variant="body2" style={{ marginBottom: '16px', fontStyle: 'italic' }}>
                    From 31/03/2022 to 31/03/2023
                </Typography>

                <Divider style={{ margin: '16px 0' }} />


                <Box display="flex" justifyContent="space-between" marginBottom="10px">
                    <Box flex="1">
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>Portfolio Value on 31/03/2022:</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>Contributions:</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>Withdrawals:</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>Realized Gains:</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>Unrealized Gains:</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>Interest:</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>Dividends:</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>Portfolio Value on 31/03/2023:</Typography>

                    </Box>
                    <Box flex="1" style={{ textAlign: 'right' }}>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>45,535.06</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>4,130,000.00</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>-2,985,000.00</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>305,332.90</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>74,302.03</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>0.00</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>0.00</Typography>
                        <Typography variant="body2" style={{ marginBottom: '10px' }}>
                            <span style={{ borderTop: '1px solid black', display: 'inline-block' }}>1,570,169.98</span>
                        </Typography>
                    </Box>
                </Box>

                <Divider style={{ margin: '16px 0' }} />

                {/* Add a margin-top for separation */}
                <Box display="flex" justifyContent="space-between" marginTop="30px" marginBottom="10px">
                    <Box flex="1">
                        <Typography variant="body2">Average Capital:</Typography>
                        <Typography variant="body2" style={{ margin: '10px 0' }}>Total Fees:</Typography>
                        <Typography variant="body2">Total Gain after Fees:</Typography>
                        <Typography variant="body2" style={{ fontWeight: 'bold', backgroundColor: '#B0B0B0' }}>IRR for 1.00 Years:</Typography>
                        <Typography variant="body2">Annualized IRR:</Typography>
                    </Box>
                    <Box flex="1" style={{ textAlign: 'right' }}>
                        <Typography variant="body2">1,451,178.89</Typography>
                        <Typography variant="body2" style={{ margin: '10px 0' }}>0.00</Typography>
                        <Typography variant="body2">379,634.92</Typography>
                        <Typography variant="body2" style={{ fontWeight: 'bold', backgroundColor: '#B0B0B0' }}>26.16%</Typography>
                        <Typography variant="body2">26.16%</Typography>
                    </Box>
                </Box>


                <Divider style={{ margin: '16px 0' }} />
            </Paper>
        </Container>
    );
}

export default PerformanceReport;
