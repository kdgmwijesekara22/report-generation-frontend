import React from 'react';
import { Typography, Container, Paper, Table, TableBody, TableCell, TableRow, Divider } from '@mui/material';

// Define types for the props (data that will be passed into the component)
interface ClientDetails {
    name: string;
    address: string;
    contactNumber: string;
    otherDetails: string;
}

interface AuditPeriod {
    startDate: string;
    endDate: string;
}

interface ConfirmationDetails {
    statement: string;
    confirmation: boolean;
    comments?: string;
}

interface AuditClientConfirmationProps {
    clientDetails: ClientDetails;
    auditPeriod: AuditPeriod;
    confirmationDetails: ConfirmationDetails[];
}

// Data from the PDF
const clientDetails = {
    name: "John Doe",
    address: "123 Main St, City, Country",
    contactNumber: "+1234567890",
    otherDetails: "Additional client details here"
};

const auditPeriod = {
    startDate: "01-01-2023",
    endDate: "31-12-2023"
};

const confirmationDetails = [
    {
        statement: "Statement 1 for confirmation",
        confirmation: true,
        comments: "Some comments here"
    },
    {
        statement: "Statement 2 for confirmation",
        confirmation: false
    },
    {
        statement: "Statement 3 for confirmation",
        confirmation: true,
        comments: "Other comments here"
    }
];

const NormalConfirmation: React.FC = () => {
    return (
        <Container maxWidth="md" style={{ textAlign: 'left', justifyContent: 'left' }}>
            <Paper elevation={3} style={{ padding: '20px', marginTop: '20px', textAlign: 'left' }}>

                <Typography variant="body1" >M.W.A.C MATHTHAMAGODA</Typography>
                <Typography variant="body1">NO 27/32, MANIKWATTHA, KALALGODA</Typography>
                <Typography variant="body1">PANNIPITIYA</Typography>
                <Typography variant="body1">Sri Lanka</Typography>
                <Typography variant="body1" style={{ marginTop: '20px' }}>Dear Sir / Madam,</Typography>

                <Typography variant="body1" gutterBottom style={{ fontWeight: 'bold', marginTop: '20px' }}>
                    CONFIRMATION OF UNITHOLDING
                </Typography>
                <Typography variant="body1" gutterBottom>
                    MATHTHAMAGODA WALAUWE ANUPA CHAMIKA MATHTHAMAGODA - 1024766-01
                </Typography>

                <Typography variant="body1" paragraph>
                    With reference to the request made by MATHTHAMAGODA WALAUWE ANUPA CHAMIKA MATHTHAMAGODA, we wish to confirm the following investments.
                </Typography>

                <Typography variant="body1" paragraph>
                    As at June 06, 2023, total number of units held in the name of MATHTHAMAGODA WALAUWE ANUPA CHAMIKA MATHTHAMAGODA, in the Unit Capital of the NDB WEALTH MANAGEMENT LIMITED Unit Trust Funds and the respective market values are as follows.
                </Typography>

                <Table style={{ borderCollapse: 'collapse' }}>
                    <TableBody>
                        <TableRow>
                            <TableCell style={{ border: '1px solid black' }}><strong>Fund Name</strong></TableCell>
                            <TableCell style={{ border: '1px solid black' }}><strong>No. of Units</strong></TableCell>
                            <TableCell style={{ border: '1px solid black' }}><strong>Unit Price (Rs.)</strong></TableCell>
                            <TableCell style={{ border: '1px solid black' }}><strong>Market Value (Rs.)</strong></TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ border: '1px solid black' }}>NDB WEALTH MONEY FUND</TableCell>
                            <TableCell style={{ border: '1px solid black' }}>53,841.87</TableCell>
                            <TableCell style={{ border: '1px solid black' }}>28.4665</TableCell>
                            <TableCell style={{ border: '1px solid black' }}>1,532,689.59</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ border: '1px solid black' }}>NDB WEALTH MONEY PLUS FUND</TableCell>
                            <TableCell style={{ border: '1px solid black' }}>48.58</TableCell>
                            <TableCell style={{ border: '1px solid black' }}>31.2783</TableCell>
                            <TableCell style={{ border: '1px solid black' }}>1,519.50</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3} style={{ border: '1px solid black', textAlign: 'right' }}><strong>Total</strong></TableCell>
                            <TableCell style={{ border: '1px solid black' }}>1,534,209.00</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>


                <Typography variant="body1" paragraph style={{ marginTop: '20px' }}>
                    NDB Wealth Funds are licensed Unit Trust Funds Governed by Securities and Exchange Commission of Sri Lanka and managed by NDB Wealth Management Limited.
                </Typography>

                <Typography variant="body1" paragraph>
                    We confirm that market value of the fund is redeemable at any given time upon the request of the client.
                </Typography>

                <Typography variant="body1" paragraph>
                    Please note that the buying price (the price a client will receive when he sells a unit to cash out) per unit is computed daily and subject to fluctuations.
                </Typography>

                <Typography variant="body1" paragraph style={{ marginTop: '20px' }}>
                    This letter has been issued on request of the above account holder without any liability on our part.
                </Typography>

                <Typography variant="body1" paragraph>
                    Yours faithfully,<br />
                    <span style={{ fontWeight: 'bold' }}>NDB WEALTH MANAGEMENT LIMITED</span>

                </Typography>


                <Typography variant="body1">
                    ...............................................<br />
                    Registrar
                </Typography>
            </Paper>
        </Container>
    );
}

export default NormalConfirmation;